'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var sassLint = require('gulp-sass-lint');



gulp.task('sass-lint', function () {
	return gulp.src(['./sass/**/*.scss', '!./_assets/sass/vendor/**/*.scss'])
		.pipe(sassLint())
		.pipe(sassLint.format())
		.pipe(sassLint.failOnError())
});


gulp.task('sass', function () {
  return gulp.src('./sass/**/*.scss')
	.pipe(sourcemaps.init())
	.pipe(sass({
		// outputStyle: 'compressed'
	}).on('error', sass.logError))
	.pipe(autoprefixer({
		browsers: ['last 2 versions', 'IE 9'],
		cascade: false
	}))
	.pipe(sourcemaps.write('.'))
	.pipe(gulp.dest('./public'));
});


// gulp.task('sass:watch', function () {
gulp.task('watch', function () {
	gulp.watch('./sass/**/*.scss', ['just-sass', 'kss']);
	// gulp.watch('./_assets/**/*', ['default']);
});


var exec = require('child_process').exec;
gulp.task('kss', function (cb) {
	exec('./node_modules/.bin/kss-node --config ./kss-config.json', function (err, stdout, stderr) {
		console.log(stdout);
		console.log(stderr);
		cb(err);
	});
});


gulp.task('default', ['sass-lint', 'sass', 'kss']);

gulp.task('just-sass', ['sass-lint', 'sass']);